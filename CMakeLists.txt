cmake_minimum_required (VERSION 2.6)
project (gpacalc)
set (CMAKE_CXX_STANDARD 11)
set (gpacalc_VERSION_MAJOR 3)
set (gpacalc_VERSION_MINOR 0)

add_executable(gpacalc src/gpacalc.cpp)
target_link_libraries(gpacalc curl)
