#include <iostream>
#include <string>
#include <vector>
#include "gradebook.cpp"
#include "update.cpp"

using namespace std;

const string CURRENT_VERSION = "3.2";

int getTotalCourses(string courseType);
vector<Grade> getGrades(vector<int> courseCount);

int main(void) {
    cout << "\n--NBPS GPA Calculator--\n";
    cout << "--Slapped together by Milo G. (Fmr. 9th Grade Secretary)--\n" << endl;

    // Chain of events:
    // 1. Look for prefs file
    // 2. Display contents, ask to load or edit
    // 3. If edit: let user make changes
    // 4. Load and display GPA
    // 5. Ask to go again or quit (in which case, check for update)

    while (true) {
        GradeBook gradebook;
        ifstream prefs("gradebook.txt");

        if (prefs) {
            cout << "Prefs file found." << endl;
            gradebook = GradeBook::deserialize();

            gradebook.edit();     
        } else {
            vector<int> courseCount; // Will be looped over to recieve grades for each course type
            
            courseCount.push_back(getTotalCourses("CP"));
            courseCount.push_back(getTotalCourses("Honors"));
            courseCount.push_back(getTotalCourses("AP/IB"));

            gradebook = GradeBook(getGrades(courseCount)); // Will calculate GPA using supplied vector

            cout << "Save your grades? 'Y' for yes or 'n' for no. ";
            char response;
            cin >> response;
            if (response == 'Y' || response == 'y') {
                int success = gradebook.serialize();
                if (success != 0) {
                    cout << "Failed when trying to save prefs." << endl;
                }
            }
        }

        cout << "\nYour GPA is: " << gradebook.gpa() << endl;
        cout << "Run the program again? Y for yes and anything else for no: ";
        char userContinue;
        cin >> userContinue;

        if (userContinue != 'Y' && userContinue != 'y') {
            int update = checkVersion(CURRENT_VERSION);
            if (update == -1) {
                cout << "Couldn't check for an update." << endl;
            } else if (update == 1) {
                cout << "\n\nA new update's here! Go to https://gitlab.com/Myl0g/gpacalc_cpp/tags\n" << endl;
            }
            return 0;
        }
    }

}

int getTotalCourses(string courseType) {
    cout << "How many " + courseType + " courses do you have a grade in?" << endl;
    int response;
    cin >> response;
    return response;
}

vector<Grade> getGrades(vector<int> courseCount) {
    vector<Grade> grades;

    for (int i = 0; i < courseCount.size(); i++) {
        for (int j = 0; j < courseCount.at(i); j++) {

            string courseType;
            if (i == 0) {
                courseType = "CP";
            } else if (i == 1) {
                courseType = "Honors";
            } else {
                courseType = "Accelerated";
            }

            cout << "Enter your grade for " << courseType << " course #" + to_string(j + 1) << endl;

            string grade;
            cin >> grade;

            const char * gradeArray = grade.c_str(); // Like substring but better
            char letter = toupper(grade[0]); // Doesn't change anything if letter is already uppercase

            char symbol;
            if (grade[1] != '+' && grade[1] != '-') {
                symbol = '0'; // Will register as 0.00 when calculating GPA
            } else {
                symbol = grade[1];
            }

            Grade gradeObj(Grade(letter, symbol, courseType));
            grades.push_back(gradeObj); // Will be passed to gradebook
        }
    }

    return grades;
}