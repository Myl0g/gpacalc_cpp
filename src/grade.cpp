#include "grade.hpp"
#include <string>

using namespace std;

Grade::Grade(char letter, char symbol, string type) {
    this->letter_rep = letter;
    switch (letter) {
        case 'A':
            this->letter = 4.00;
            break;
        case 'B':
            this->letter = 3.00;
            break;
        case 'C':
            this->letter = 2.00;
            break;
        case 'D':
            this->letter = 1.00;
            break;
        default:
            this->letter = 0.00;
            break;
    }

    this->symbol_rep = symbol;
    switch (symbol) {
        case '+':
            this->symbol = 0.33;
            break;
        case '-':
            this->symbol = -0.33;
            break;
        default:
            this->symbol = 0;
            break;
    }

    if (type == "CP") {
        this->type = 1.00;
        this->type_rep = 'C';
    } else if (type == "Honors") {
        this->type = 1.15;
        this->type_rep = 'H';
    } else if (type == "Accelerated") {
        this->type = 1.30;
        this->type_rep = 'A';
    }
}

Grade::Grade(string letter, double symbol, double type) {
    this->letter = letter.c_str()[0];
    this->symbol = symbol;
    this->type = type;
}

string Grade::serialize() const {
    return "" + letter_rep + symbol_rep + type_rep;
}